package demo.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class LoginErrorsController {

	@RequestMapping("/dashboard/login")
	public String dashboard() {
		// Redirect to AngularJS
		return "redirect:/#/";
	}
}
