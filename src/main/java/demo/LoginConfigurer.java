package demo;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Configure Spring Security
 *
 */
// Spring Boot attaches a special meaning to a WebSecurityConfigurer
// on a class that carries the @EnableOAuth2Sso annotation. It uses it to configure
// the security filter chain that carries the OAuth2 authentication processor.
@Component
@EnableOAuth2Sso
public class LoginConfigurer extends WebSecurityConfigurerAdapter {

	/**
	 * Make the Dashboard web page and static resources that it contains, as
	 * well as the login endpoints which handle authentication,
	 * accessible without the user being authenticated. All other requests
	 * require authentication.
	 */
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.antMatcher("/dashboard/**")
			.authorizeRequests()
				.anyRequest().authenticated().and().csrf().csrfTokenRepository(csrfTokenRepository())
				.and().addFilterAfter(csrfHeaderFilter(), CsrfFilter.class)
			.logout().logoutUrl("/dashboard/logout").permitAll().logoutSuccessUrl("/");
	}

	/**
	 * This method is used by the configure() method. 
	 * 
	 * AngularJS has built in support for CSRF (called XSRF in AngularJS), but
	 * it is implemented in a slightly different way than the default behavior of Spring Security.
	 * Angular would like the server to send a cookie called "XSRF-TOKEN".
	 * When Angular sees this cookie, it will send the value back as a header named "X-XSRF-TOKEN".
	 * Spring Security is configured here use a filter that creates the cookie and 
	 * to tell the existing CRSF filter about the header name. 
	 */
	private Filter csrfHeaderFilter() {
		return new OncePerRequestFilter() {
			@Override
			protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
					FilterChain filterChain) throws ServletException, IOException {
				CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
				if (csrf != null) {
					Cookie cookie = new Cookie("XSRF-TOKEN", csrf.getToken());
					cookie.setPath("/");
					response.addCookie(cookie);
				}
				filterChain.doFilter(request, response);
			}
		};
	}

	private CsrfTokenRepository csrfTokenRepository() {
		HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
		repository.setHeaderName("X-XSRF-TOKEN");
		return repository;
	}
}
