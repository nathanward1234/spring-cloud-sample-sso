package demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * See also:
 * https://github.com/spring-cloud-samples/sso
 * https://github.com/spring-cloud-samples/authserver
 * https://spring.io/guides/tutorials/spring-boot-oauth2/
 *
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class SsoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SsoApplication.class, args);
	}

}
